import { Component, Input, OnInit } from '@angular/core';
import axios from 'axios';
import { SharedService } from '../shared.service';

@Component({
    selector: 'app-check-list',
    templateUrl: './check-list.component.html',
    styleUrls: ['./check-list.component.css']
})
export class CheckListComponent implements OnInit {
    @Input()
    getId: string = "";

    apiKey = "512b806d37d0b5db167074f631eed3e7"
    apiToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"

    cardData = [{
        "id": "6460fe897d6e2722c2276082",
        "badges": {
            "attachmentsByType": {
                "trello": {
                    "board": 0,
                    "card": 0
                }
            },
            "location": false,
            "votes": 0,
            "viewingMemberVoted": false,
            "subscribed": false,
            "fogbugz": "",
            "checkItems": 1,
            "checkItemsChecked": 0,
            "checkItemsEarliestDue": null,
            "comments": 0,
            "attachments": 0,
            "description": false,
            "due": null,
            "dueComplete": false,
            "start": null
        },
        "checkItemStates": null,
        "closed": false,
        "dueComplete": false,
        "dateLastActivity": "2023-05-14T15:30:49.233Z",
        "desc": "",
        "descData": {
            "emoji": {}
        },
        "due": null,
        "dueReminder": null,
        "email": null,
        "idBoard": "6460db61f269b7912547f00e",
        "idChecklists": [
            "6460fea473877a99036a6c32"
        ],
        "idList": "6460fe3afc05724a9b4356c8",
        "idMembers": [],
        "idMembersVoted": [],
        "idShort": 1,
        "idAttachmentCover": null,
        "labels": [],
        "idLabels": [],
        "manualCoverAttachment": false,
        "name": "",
        "pos": 65535,
        "shortLink": "EcfQykCf",
        "shortUrl": "https://trello.com/c/EcfQykCf",
        "start": null,
        "subscribed": false,
        "url": "https://trello.com/c/EcfQykCf/1-uhujh",
        "cover": {
            "idAttachment": null,
            "color": null,
            "idUploadedBackground": null,
            "size": "normal",
            "brightness": "dark",
            "idPlugin": null
        },
        "isTemplate": false,
        "cardRole": null
    }]

    constructor(private sharedService: SharedService) { }

    itemData = [];
    cardId: string = "";

    ngOnInit() {

        return axios.get(`https://api.trello.com/1/lists/${this.getId}/cards?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => this.cardData = response)
            .catch(error => console.log("Error occured in fetching cards" + error))
    }

    create(cardName: string) {
        return axios.post(`https://api.trello.com/1/cards?name=${cardName}&idList=${this.getId}&key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => window.location.reload())
            .catch(error => console.log("Error occured in creating card" + error))
    }

    delete(id: string) {
        return axios.delete(`https://api.trello.com/1/cards/${id}?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => window.location.reload())
            .catch(error => console.log("Error occured in deleting cards" + error))
    }

    createItem(checkListName: string, id: string) {
        return axios.post(`https://api.trello.com/1/checklists?name=${checkListName}&idCard=${id}&key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .catch(error => console.log("Error occured while creating checklists " + error))


    }
    displayItems(id: string) {
        this.sharedService.cardId = id;
    }

}
