import { Component } from '@angular/core';
import axios from 'axios';
import { SharedService } from '../shared.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

    apiKey = "512b806d37d0b5db167074f631eed3e7"
    apiToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"

    constructor(private sharedService: SharedService, private location: Location) { }

    createBoard(boardName: string) {

        return axios.post(`https://api.trello.com/1/boards/?name=${boardName}&key=${this.apiKey}&token=${this.apiToken}`).then(response => response.data)
            .then(response => window.location.reload())
            .catch(error => console.log("Error occured in creating board" + error))

    }
}
