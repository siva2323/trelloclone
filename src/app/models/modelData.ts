export type Root = Root2[]

export interface Root2 {
  id: string
  nodeId: string
  name: string
  desc: string
  descData: any
  closed: boolean
  dateClosed: any
  idOrganization: string
  idEnterprise: any
  limits: Limits
  pinned: boolean
  starred: boolean
  url: string
  prefs: Prefs
  shortLink: string
  subscribed: boolean
  labelNames: LabelNames
  powerUps: any[]
  dateLastActivity: any
  dateLastView: any
  shortUrl: string
  idTags: any[]
  datePluginDisable: any
  creationMethod: string
  ixUpdate: string
  templateGallery: any
  enterpriseOwned: boolean
  idBoardSource: any
  premiumFeatures: string[]
  idMemberCreator: string
  memberships: Membership[]
}

export interface Limits {
  attachments: Attachments
  boards: Boards
  cards: Cards
  checklists: Checklists
  checkItems: CheckItems
  customFields: CustomFields
  customFieldOptions: CustomFieldOptions
  labels: Labels
  lists: Lists
  stickers: Stickers
  reactions: Reactions
}

export interface Attachments {
  perBoard: PerBoard
  perCard: PerCard
}

export interface PerBoard {
  status: string
  disableAt: number
  warnAt: number
}

export interface PerCard {
  status: string
  disableAt: number
  warnAt: number
}

export interface Boards {
  totalMembersPerBoard: TotalMembersPerBoard
  totalAccessRequestsPerBoard: TotalAccessRequestsPerBoard
}

export interface TotalMembersPerBoard {
  status: string
  disableAt: number
  warnAt: number
}

export interface TotalAccessRequestsPerBoard {
  status: string
  disableAt: number
  warnAt: number
}

export interface Cards {
  openPerBoard: OpenPerBoard
  openPerList: OpenPerList
  totalPerBoard: TotalPerBoard
  totalPerList: TotalPerList
}

export interface OpenPerBoard {
  status: string
  disableAt: number
  warnAt: number
}

export interface OpenPerList {
  status: string
  disableAt: number
  warnAt: number
}

export interface TotalPerBoard {
  status: string
  disableAt: number
  warnAt: number
}

export interface TotalPerList {
  status: string
  disableAt: number
  warnAt: number
}

export interface Checklists {
  perBoard: PerBoard2
  perCard: PerCard2
}

export interface PerBoard2 {
  status: string
  disableAt: number
  warnAt: number
}

export interface PerCard2 {
  status: string
  disableAt: number
  warnAt: number
}

export interface CheckItems {
  perChecklist: PerChecklist
}

export interface PerChecklist {
  status: string
  disableAt: number
  warnAt: number
}

export interface CustomFields {
  perBoard: PerBoard3
}

export interface PerBoard3 {
  status: string
  disableAt: number
  warnAt: number
}

export interface CustomFieldOptions {
  perField: PerField
}

export interface PerField {
  status: string
  disableAt: number
  warnAt: number
}

export interface Labels {
  perBoard: PerBoard4
}

export interface PerBoard4 {
  status: string
  disableAt: number
  warnAt: number
}

export interface Lists {
  openPerBoard: OpenPerBoard2
  totalPerBoard: TotalPerBoard2
}

export interface OpenPerBoard2 {
  status: string
  disableAt: number
  warnAt: number
}

export interface TotalPerBoard2 {
  status: string
  disableAt: number
  warnAt: number
}

export interface Stickers {
  perCard: PerCard3
}

export interface PerCard3 {
  status: string
  disableAt: number
  warnAt: number
}

export interface Reactions {
  perAction: PerAction
  uniquePerAction: UniquePerAction
}

export interface PerAction {
  status: string
  disableAt: number
  warnAt: number
}

export interface UniquePerAction {
  status: string
  disableAt: number
  warnAt: number
}

export interface Prefs {
  permissionLevel: string
  hideVotes: boolean
  voting: string
  comments: string
  invitations: string
  selfJoin: boolean
  cardCovers: boolean
  isTemplate: boolean
  cardAging: string
  calendarFeedEnabled: boolean
  hiddenPluginBoardButtons: any[]
  switcherViews: SwitcherView[]
  background: string
  backgroundColor: string
  backgroundImage: any
  backgroundImageScaled: any
  backgroundTile: boolean
  backgroundBrightness: string
  backgroundBottomColor: string
  backgroundTopColor: string
  canBePublic: boolean
  canBeEnterprise: boolean
  canBeOrg: boolean
  canBePrivate: boolean
  canInvite: boolean
}

export interface SwitcherView {
  viewType: string
  enabled: boolean
  _id: string
  typeName: string
  id: string
}

export interface LabelNames {
  green: string
  yellow: string
  orange: string
  red: string
  purple: string
  blue: string
  sky: string
  lime: string
  pink: string
  black: string
  green_dark: string
  yellow_dark: string
  orange_dark: string
  red_dark: string
  purple_dark: string
  blue_dark: string
  sky_dark: string
  lime_dark: string
  pink_dark: string
  black_dark: string
  green_light: string
  yellow_light: string
  orange_light: string
  red_light: string
  purple_light: string
  blue_light: string
  sky_light: string
  lime_light: string
  pink_light: string
  black_light: string
}

export interface Membership {
  idMember: string
  memberType: string
  unconfirmed: boolean
  deactivated: boolean
  id: string
}
