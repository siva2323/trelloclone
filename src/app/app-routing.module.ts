import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { AllBoardsComponent } from './all-boards/all-boards.component';
import { BoardComponent } from './board/board.component';

const routes: Routes = [
    { path: "", component: HomepageComponent },
    { path: `boards/:boardId`, component: BoardComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
