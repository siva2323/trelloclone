import { Component } from '@angular/core';

@Component({
    selector: 'app-expansion',
    templateUrl: './expansion.component.html',
    styleUrls: ['./expansion.component.css']
})
export class ExpansionComponent {
    panelOpenState = false;
    typesOfItems: string[] = ['📂 Boards', '📋 Highlights', '✔ Views', "👩 Members", "🔧 Settings"];
}
