import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardSideNavComponent } from './board-side-nav.component';

describe('BoardSideNavComponent', () => {
  let component: BoardSideNavComponent;
  let fixture: ComponentFixture<BoardSideNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BoardSideNavComponent]
    });
    fixture = TestBed.createComponent(BoardSideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
