import { Component } from '@angular/core';

@Component({
  selector: 'app-board-side-nav',
  templateUrl: './board-side-nav.component.html',
  styleUrls: ['./board-side-nav.component.css']
})
export class BoardSideNavComponent {
    typesOfItems: string[] = ['ㄖ Boards', '❏ Templates', '﹌ Home']
    typesOfItem: string[] = ['📋 Tables', '📋 Calendar'];
}
