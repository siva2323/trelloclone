import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { Location } from '@angular/common';
import axios from 'axios';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-all-list',
    templateUrl: './all-list.component.html',
    styleUrls: ['./all-list.component.css']
})
export class AllListComponent implements OnInit {

    listData = [{
        "id": "6460db61f269b7912547f015",
        "name": "",
        "closed": false,
        "idBoard": "6460db61f269b7912547f00e",
        "pos": 16384,
        "subscribed": false,
        "softLimit": null,
        "status": null
    }];

    constructor(private sharedService: SharedService, private location: Location, private route: ActivatedRoute) { }

    boardId: string = "";
    apiKey = "512b806d37d0b5db167074f631eed3e7"
    apiToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"
    checklistData = [{
        "id": "6460fe897d6e2722c2276082",
        "badges": {
            "attachmentsByType": {
                "trello": {
                    "board": 0,
                    "card": 0
                }
            },
            "location": false,
            "votes": 0,
            "viewingMemberVoted": false,
            "subscribed": false,
            "fogbugz": "",
            "checkItems": 1,
            "checkItemsChecked": 0,
            "checkItemsEarliestDue": null,
            "comments": 0,
            "attachments": 0,
            "description": false,
            "due": null,
            "dueComplete": false,
            "start": null
        },
        "checkItemStates": null,
        "closed": false,
        "dueComplete": false,
        "dateLastActivity": "2023-05-14T15:30:49.233Z",
        "desc": "",
        "descData": {
            "emoji": {}
        },
        "due": null,
        "dueReminder": null,
        "email": null,
        "idBoard": "6460db61f269b7912547f00e",
        "idChecklists": [
            "6460fea473877a99036a6c32"
        ],
        "idList": "6460fe3afc05724a9b4356c8",
        "idMembers": [],
        "idMembersVoted": [],
        "idShort": 1,
        "idAttachmentCover": null,
        "labels": [],
        "idLabels": [],
        "manualCoverAttachment": false,
        "name": "uhujh",
        "pos": 65535,
        "shortLink": "EcfQykCf",
        "shortUrl": "https://trello.com/c/EcfQykCf",
        "start": null,
        "subscribed": false,
        "url": "https://trello.com/c/EcfQykCf/1-uhujh",
        "cover": {
            "idAttachment": null,
            "color": null,
            "idUploadedBackground": null,
            "size": "normal",
            "brightness": "dark",
            "idPlugin": null
        },
        "isTemplate": false,
        "cardRole": null
    }]



    ngOnInit() {
        this.route.params.subscribe(params => {
            const id = params['boardId'];
            this.boardId = id;
        });

        return axios.get(`https://api.trello.com/1/boards/${this.boardId}/lists?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => this.listData = response)
            .catch(error => console.log("Error occured in fetching Lists" + error))
    }

    create(listName: string) {
        return axios.post(`https://api.trello.com/1/boards/${this.boardId}/lists?name=${listName}&key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => window.location.reload())
            .catch(error => console.log("Error occured in creating Lists" + error))

    }

    delete(id: string) {
        return axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => window.location.reload())
            .catch(error => console.log("Error occured while deleting list" + error))
    }

}
