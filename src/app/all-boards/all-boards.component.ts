import { Component, OnInit } from '@angular/core';
import { Root } from "../models/modelData"
import { SharedService } from '../shared.service';

@Component({
    selector: 'app-all-boards',
    templateUrl: './all-boards.component.html',
    styleUrls: ['./all-boards.component.css']
})
export class AllBoardsComponent implements OnInit {

    boardsData: Root = [{
        "id": "645e2a551856e6c523cf6cbb",
        "nodeId": "ari:cloud:trello::board/workspace/645e0e3435ac413da8827946/645e2a551856e6c523cf6cbb",
        "name": "",
        "desc": "",
        "descData": null,
        "closed": false,
        "dateClosed": null,
        "idOrganization": "645e0e3435ac413da8827946",
        "idEnterprise": null,
        "limits": {
            "attachments": {
                "perBoard": {
                    "status": "ok",
                    "disableAt": 36000,
                    "warnAt": 28800
                },
                "perCard": {
                    "status": "ok",
                    "disableAt": 1000,
                    "warnAt": 800
                }
            },
            "boards": {
                "totalMembersPerBoard": {
                    "status": "ok",
                    "disableAt": 1600,
                    "warnAt": 1280
                },
                "totalAccessRequestsPerBoard": {
                    "status": "ok",
                    "disableAt": 4000,
                    "warnAt": 3200
                }
            },
            "cards": {
                "openPerBoard": {
                    "status": "ok",
                    "disableAt": 5000,
                    "warnAt": 4000
                },
                "openPerList": {
                    "status": "ok",
                    "disableAt": 5000,
                    "warnAt": 4000
                },
                "totalPerBoard": {
                    "status": "ok",
                    "disableAt": 2000000,
                    "warnAt": 1600000
                },
                "totalPerList": {
                    "status": "ok",
                    "disableAt": 1000000,
                    "warnAt": 800000
                }
            },
            "checklists": {
                "perBoard": {
                    "status": "ok",
                    "disableAt": 1800000,
                    "warnAt": 1440000
                },
                "perCard": {
                    "status": "ok",
                    "disableAt": 500,
                    "warnAt": 400
                }
            },
            "checkItems": {
                "perChecklist": {
                    "status": "ok",
                    "disableAt": 200,
                    "warnAt": 160
                }
            },
            "customFields": {
                "perBoard": {
                    "status": "ok",
                    "disableAt": 50,
                    "warnAt": 40
                }
            },
            "customFieldOptions": {
                "perField": {
                    "status": "ok",
                    "disableAt": 50,
                    "warnAt": 40
                }
            },
            "labels": {
                "perBoard": {
                    "status": "ok",
                    "disableAt": 1000,
                    "warnAt": 800
                }
            },
            "lists": {
                "openPerBoard": {
                    "status": "ok",
                    "disableAt": 500,
                    "warnAt": 400
                },
                "totalPerBoard": {
                    "status": "ok",
                    "disableAt": 3000,
                    "warnAt": 2400
                }
            },
            "stickers": {
                "perCard": {
                    "status": "ok",
                    "disableAt": 70,
                    "warnAt": 56
                }
            },
            "reactions": {
                "perAction": {
                    "status": "ok",
                    "disableAt": 900,
                    "warnAt": 720
                },
                "uniquePerAction": {
                    "status": "ok",
                    "disableAt": 17,
                    "warnAt": 14
                }
            }
        },
        "pinned": false,
        "starred": false,
        "url": "https://trello.com/b/rSt5920H/sdfs",
        "prefs": {
            "permissionLevel": "private",
            "hideVotes": false,
            "voting": "disabled",
            "comments": "members",
            "invitations": "members",
            "selfJoin": true,
            "cardCovers": true,
            "isTemplate": false,
            "cardAging": "regular",
            "calendarFeedEnabled": false,
            "hiddenPluginBoardButtons": [],
            "switcherViews": [
                {
                    "viewType": "Board",
                    "enabled": true,
                    "_id": "645e2a551856e6c523cf6cbc",
                    "typeName": "SwitcherViews",
                    "id": "645e2a551856e6c523cf6cbc"
                },
                {
                    "viewType": "Table",
                    "enabled": true,
                    "_id": "645e2a551856e6c523cf6cbd",
                    "typeName": "SwitcherViews",
                    "id": "645e2a551856e6c523cf6cbd"
                },
                {
                    "viewType": "Calendar",
                    "enabled": false,
                    "_id": "645e2a551856e6c523cf6cbe",
                    "typeName": "SwitcherViews",
                    "id": "645e2a551856e6c523cf6cbe"
                },
                {
                    "viewType": "Dashboard",
                    "enabled": false,
                    "_id": "645e2a551856e6c523cf6cbf",
                    "typeName": "SwitcherViews",
                    "id": "645e2a551856e6c523cf6cbf"
                },
                {
                    "viewType": "Timeline",
                    "enabled": false,
                    "_id": "645e2a551856e6c523cf6cc0",
                    "typeName": "SwitcherViews",
                    "id": "645e2a551856e6c523cf6cc0"
                },
                {
                    "viewType": "Map",
                    "enabled": false,
                    "_id": "645e2a551856e6c523cf6cc1",
                    "typeName": "SwitcherViews",
                    "id": "645e2a551856e6c523cf6cc1"
                }
            ],
            "background": "blue",
            "backgroundColor": "#0079BF",
            "backgroundImage": null,
            "backgroundImageScaled": null,
            "backgroundTile": false,
            "backgroundBrightness": "dark",
            "backgroundBottomColor": "#0079BF",
            "backgroundTopColor": "#0079BF",
            "canBePublic": true,
            "canBeEnterprise": true,
            "canBeOrg": true,
            "canBePrivate": true,
            "canInvite": true
        },
        "shortLink": "rSt5920H",
        "subscribed": false,
        "labelNames": {
            "green": "",
            "yellow": "",
            "orange": "",
            "red": "",
            "purple": "",
            "blue": "",
            "sky": "",
            "lime": "",
            "pink": "",
            "black": "",
            "green_dark": "",
            "yellow_dark": "",
            "orange_dark": "",
            "red_dark": "",
            "purple_dark": "",
            "blue_dark": "",
            "sky_dark": "",
            "lime_dark": "",
            "pink_dark": "",
            "black_dark": "",
            "green_light": "",
            "yellow_light": "",
            "orange_light": "",
            "red_light": "",
            "purple_light": "",
            "blue_light": "",
            "sky_light": "",
            "lime_light": "",
            "pink_light": "",
            "black_light": ""
        },
        "powerUps": [],
        "dateLastActivity": null,
        "dateLastView": null,
        "shortUrl": "https://trello.com/b/rSt5920H",
        "idTags": [],
        "datePluginDisable": null,
        "creationMethod": "automatic",
        "ixUpdate": "3",
        "templateGallery": null,
        "enterpriseOwned": false,
        "idBoardSource": null,
        "premiumFeatures": [
            "additionalBoardBackgrounds",
            "additionalStickers",
            "customBoardBackgrounds",
            "customEmoji",
            "customStickers",
            "plugins"
        ],
        "idMemberCreator": "5e6cc397e044ff8e6ef37d9b",
        "memberships": [
            {
                "idMember": "5e6cc397e044ff8e6ef37d9b",
                "memberType": "admin",
                "unconfirmed": false,
                "deactivated": false,
                "id": "645e2a551856e6c523cf6cc6"
            }
        ]
    }]

    constructor(private sharedService: SharedService) { }

    ngOnInit() {
        this.sharedService.ngOnInit().then(response => this.boardsData = this.sharedService.boardsData)
    }

    goToBoardDetails(boardId: string) {
        this.sharedService.goToBoardDetails(boardId)
    }

}
