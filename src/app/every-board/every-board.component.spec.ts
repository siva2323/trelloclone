import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EveryBoardComponent } from './every-board.component';

describe('EveryBoardComponent', () => {
  let component: EveryBoardComponent;
  let fixture: ComponentFixture<EveryBoardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EveryBoardComponent]
    });
    fixture = TestBed.createComponent(EveryBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
