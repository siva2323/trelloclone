import { Component } from '@angular/core';
import axios from 'axios';

@Component({
    selector: 'app-every-board',
    templateUrl: './every-board.component.html',
    styleUrls: ['./every-board.component.css']
})
export class EveryBoardComponent {

    APIKey = "512b806d37d0b5db167074f631eed3e7"
    APIToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"


    deleteBoard(id: number) {
        return axios.delete(`https://api.trello.com/1/boards/${id}?key=${this.APIKey}&token=${this.APIToken}`)
            .catch(error => console.log("Error occured in deleting board" + error))
    }


}
