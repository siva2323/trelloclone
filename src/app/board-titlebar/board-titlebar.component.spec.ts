import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardTitlebarComponent } from './board-titlebar.component';

describe('BoardTitlebarComponent', () => {
  let component: BoardTitlebarComponent;
  let fixture: ComponentFixture<BoardTitlebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BoardTitlebarComponent]
    });
    fixture = TestBed.createComponent(BoardTitlebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
