import { Component, OnInit, ViewChild } from '@angular/core';
import { Input } from '@angular/core';
import { Location } from '@angular/common';
import { SharedService } from '../shared.service';
import axios from 'axios';

@Component({
    selector: 'app-board-titlebar',
    templateUrl: './board-titlebar.component.html',
    styleUrls: ['./board-titlebar.component.css']
})
export class BoardTitlebarComponent implements OnInit {


    constructor(private sharedService: SharedService) { }


    itemData = [{
        "id": "646195d0e4bf4f6c8fad0e6f",
        "name": "",
        "idBoard": "6460db61f269b7912547f00e",
        "idCard": "6460fe897d6e2722c2276082",
        "pos": 65536,
        "checkItems": [],
        "limits": {}
    }];

    refreshId: string = "";

    tasksData = [{
        "id": "646195d0e4bf4f6c8fad0e6f",
        "name": "",
        "idBoard": "6460db61f269b7912547f00e",
        "idCard": "6460fe897d6e2722c2276082",
        "pos": 65536,
        "checkItems": [],
        "limits": {},
        "idChecklist": ""
    }];
    data = [{
        "id": "646195d0e4bf4f6c8fad0e6f",
        "name": "",
        "idBoard": "6460db61f269b7912547f00e",
        "idCard": "6460fe897d6e2722c2276082",
        "pos": 65536,
        "checkItems": [],
        "limits": {},
        "idChecklist": ""
    }];
    apiKey = "512b806d37d0b5db167074f631eed3e7"
    apiToken = "ATTAbeec5eaa8a7337fab5d2c505dafe1d14c531be3c8247e6802a8ddc185955220cD98D4C3B"

    ngOnInit() {
    }

    getItems() {
        return axios.get(`https://api.trello.com/1/cards/${this.sharedService.cardId}/checklists?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => this.itemData = response)
            .then(response => this.tasksData=this.data)
            .catch(error => console.log("Error occured while getting checklists " + error))
    }

    create(checkListName: string, id: string) {
        return axios.post(`https://api.trello.com/1/checklists?name=${checkListName}&idCard=${this.sharedService.cardId}&key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => this.getItems())
            .catch(error => console.log("Error occured while creating checklists " + error))

    }

    delete(id: string) {
        return axios.delete(`https://api.trello.com/1/checklists/${id}?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => this.getItems())
            .catch(error => console.log("Error occured while deleting checklists " + error))

    }

    getTasks(id: string) {
        this.refreshId = id;
        return axios.get(`https://api.trello.com/1/checklists/${id}/checkItems?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => this.tasksData = response)
            .then(response => console.log(this.tasksData))
            .catch(error => console.log("Error occured in getting items" + error))

    }

    createTask(taskName: string) {
        return axios.post(`https://api.trello.com/1/checklists/${this.refreshId}/checkItems?name=${taskName}&key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => response.data)
            .then(response => this.getTasks(this.refreshId))
            .catch(error => console.log("Error occured in creating items " + error))

    }

    deleteTask(itemId: string, taskId: string) {
        return axios.delete(`https://api.trello.com/1/checklists/${itemId}/checkItems/${taskId}?key=${this.apiKey}&token=${this.apiToken}`)
            .then(response => this.getTasks(this.refreshId))
            .catch(error => console.log("error occured in deleting items " + error))

    }

}
