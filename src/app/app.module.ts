import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AllBoardsComponent } from './all-boards/all-boards.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainbarComponent } from './mainbar/mainbar.component';
import { FormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from "@angular/material/expansion";
import { ExpansionComponent } from './expansion/expansion.component';
import { RecentlyViewedComponent } from './recently-viewed/recently-viewed.component';
import { BoardComponent } from './board/board.component';
import { EveryBoardComponent } from './every-board/every-board.component'
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import { BoardSideNavComponent } from './board-side-nav/board-side-nav.component';
import { BoardTitlebarComponent } from './board-titlebar/board-titlebar.component';
import { AllListComponent } from './all-list/all-list.component'
import { MatCardModule} from "@angular/material/card"
import { Router } from '@angular/router';
import { CheckListComponent } from './check-list/check-list.component';
import { FinalItemComponent } from './final-item/final-item.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    AllBoardsComponent,
    NavbarComponent,
    SidebarComponent,
    MainbarComponent,
    ExpansionComponent,
    RecentlyViewedComponent,
    BoardComponent,
    EveryBoardComponent,
    BoardSideNavComponent,
    BoardTitlebarComponent,
    AllListComponent,
    CheckListComponent,
    FinalItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatListModule,
    MatExpansionModule,
    MatSidenavModule,
    MatIconModule,
    MatCardModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
